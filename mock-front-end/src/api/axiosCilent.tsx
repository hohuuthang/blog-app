import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import queryString from "query-string";
import { toastError } from "../logic/functionList";

const axiosClient = axios.create({
  baseURL: "http://localhost:3000/api",
  headers: {
    "Content-Type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.request.use(
  function (config: AxiosRequestConfig) {
    const customHeaders: any = {};
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      customHeaders.Authorization = "Bearer " + accessToken;
    }
    return {
      ...config,
      headers: {
        ...customHeaders, // auto attach token
        ...config.headers, // but you can override for some requests
      },
    };
  },
  function (error: AxiosError) {
    return Promise.reject(error);
  }
);

axiosClient.interceptors.response.use(
  function (response: AxiosResponse) {
    return response.data;
  },
  function (error: AxiosError) {
    toastError(error.response?.data.errors)
    return Promise.reject(error.response?.data);
  }
);

export default axiosClient;
