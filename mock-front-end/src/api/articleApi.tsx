import axiosClient from "./axiosCilent";

const articleApi = {
  getListArticle: (params: any) => {
    const url = "/articles";
    return axiosClient.get(url, { params });
  },
  feedArticle: (params: any) => {
    const url = "/articles/feed";
    return axiosClient.get(url, { params });
  },
  getOneArticle: (slug: string) => {
    const url = "/articles/" + slug;
    return axiosClient.get(url);
  },
  createArticle: (articleData: any) => {
    const url = "/articles";
    return axiosClient.post(url, { article: articleData });
  },
  updateArticle: (articleData: any, slug: string) => {
    const url = "/articles/" + slug;
    return axiosClient.put(url, { article: articleData });
  },
  deleteArticle: (slug: string) => {
    const url = "/articles/" + slug;
    return axiosClient.delete(url);
  },
  favoriteArticle: (slug: string) => {
    const url = `/articles/${slug}/favorite`;
    return axiosClient.post(url);
  },
  unfavoriteArticle: (slug: string) => {
    const url = `/articles/${slug}/favorite`;
    return axiosClient.delete(url);
  },
};
export default articleApi;
