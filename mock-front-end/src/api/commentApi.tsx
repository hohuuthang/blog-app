import axiosClient from "./axiosCilent";

const commentApi = {
  addCommentToArticle: (commentData: any, slug: string) => {
    const url = `/articles/${slug}/comments`;
    return axiosClient.post(url, { comment: commentData });
  },
  getCommentFromArticle: (slug: string) => {
    const url = `/articles/${slug}/comments`;
    return axiosClient.get(url);
  },
  deleteComment: (slug: string, id: any) => {
    const url = `/articles/${slug}/comments/${id}`;
    return axiosClient.delete(url);
  },
};
export default commentApi;
