import axiosClient from "./axiosCilent";

const userApi = {
  login: (userData: any) => {
    const url = "/users/login";
    return axiosClient.post(url, { user: userData });
  },
  registration: (userData: any) => {
    const url = "/users";
    return axiosClient.post(url, { user: userData });
  },
  getCurrentUser: () => {
    const url = "/user";
    return axiosClient.get(url);
  },
  updateUser: (userData: any) => {
    const url = "/user";
    return axiosClient.put(url, { user: userData });
  },
  getProfileByUserName: (username: string) => {
    const url = "/profiles/" + username;
    return axiosClient.get(url);
  },
  followUserByUserName: (username: string) => {
    const url = `/profiles/${username}/follow`;
    return axiosClient.post(url);
  },
  unfollowUserByUserName: (username: string) => {
    const url = `/profiles/${username}/follow`;
    return axiosClient.delete(url);
  },
};
export default userApi;
