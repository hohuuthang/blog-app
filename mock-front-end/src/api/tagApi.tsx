// import axios from "axios";
import axiosClient from "./axiosCilent";

const tagApi = {
  getListOfTags: () => {
    const url = "/tags";
    return axiosClient.get(url);
  },
};
export default tagApi;
