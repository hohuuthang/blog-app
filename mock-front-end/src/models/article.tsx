export interface Author{
    username:string,
    bio:string,
    image:string,
    following:boolean
}
export interface PaginationParams{
    _limit:number,
    _page:number,
    _total:number
}
export interface Articles{
    slug:string,
    title:string,
    description:string,
    body:string,
    tagList:string[],
    favorite:boolean;
    favoritesCount:number,
    author:Author,
    pagination:PaginationParams  
}