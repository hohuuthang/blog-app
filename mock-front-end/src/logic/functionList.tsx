import { toast } from "react-toastify";

export const toDate = function (str: any) {
  var date = new Date(str);
  var year = date.getFullYear();
  var month: any = date.getMonth() + 1;
  var dt: any = date.getDate();
  if (dt < 10) {
    dt = "0" + dt;
  }
  if (month < 10) {
    month = "0" + month;
  }
  return dt + "/" + month + "/" + year;
};
export function caculateNumOfPage(lengthOfArr: number, size: number): number {
  let result;
  if (lengthOfArr < size) result = 0;
  else {
    if (lengthOfArr % size === 0) result = lengthOfArr / size;
    else result = Math.floor(lengthOfArr / size) + 1;
  }
  return result;
}

export function toastError(error: any) {
  const message = Object.keys(error) + "" + error[Object.keys(error)[0]];
  if (message !== null && typeof message !== "undefined" && message !== "") {
    toast.error(message);
  }
}
export function toastSuccess(message: any) {
  toast.success(message);
}

export function getListCommentPaging(list: any, pageIndex: any, size: any) {
  let outputList = [];
  let firstIndex = (pageIndex - 1) * size;
  let secondIndex = (pageIndex - 1) * size + size;
  for (let i = firstIndex; i < secondIndex; i++) {
    if (list[i] !== undefined) outputList.push(list[i]);
  }
  console.log(outputList);
  return outputList;
}
