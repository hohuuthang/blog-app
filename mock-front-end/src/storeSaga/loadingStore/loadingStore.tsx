const int: any = {
  loadingPage: false,
  loadingTags: false,
  loadingArticles: false,
  loadingArticlesProfile: false,
  loadingComments: false,
};

const loadingReducer = (state = int, action: any) => {
  switch (action.type) {
    case "REGISTER":
    case "LOGIN":
      return {
        ...state,
        loadingPage: true,
      };
    case "END_LOADING":
      return {
        ...state,
        loadingPage: false,
      };
    case "LOADING_TAGS":
      return {
        ...state,
        loadingTags: true,
      };
    case "END_LOADING_TAGS":
      return {
        ...state,
        loadingTags: false,
      };
    case "GLOBAL_ARTICLES":
    case "YOUR_FEED":
      return {
        ...state,
        loadingArticles: true,
      };
    case "END_LOADING_ARTICLE":
      return {
        ...state,
        loadingArticles: false,
      };
    case "LOADING_ARTICLE_PROFILES":
      return {
        ...state,
        loadingArticlesProfile: true,
      };
    case "END_LOADING_ARTICLE_PROFILES":
      return {
        ...state,
        loadingArticlesProfile: false,
      };
    case "LOADING_COMMENTS":
      return {
        ...state,
        loadingComments: true,
      };
    case "END_LOADING_COMMENTS":
      return {
        ...state,
        loadingComments: false,
      };
    default:
      return state;
  }
};

export default loadingReducer;
