import { configureStore} from "@reduxjs/toolkit";
import createSagaMiddleware from "@redux-saga/core";
import rootSaga from "./rootSaga";
import userReducer from "./userStore/userSlice";
import articlesReducer from "./articleStore/articlesSlice";
import loadingReducer from "./loadingStore/loadingStore";

// create saga middleware
const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    user: userReducer,
    articles: articlesReducer,
    loading: loadingReducer
  },
  middleware: (getDefaultMiddleware) =>
    // mount it to the store
    getDefaultMiddleware().concat(sagaMiddleware),
});

// then run the saga
sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
