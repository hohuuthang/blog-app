export const loginUser = (user: any) => {
  return {
    type: "LOGIN",
    payload: user,
  };
};
export const registerUser = (user: any) => {
  return {
    type: "REGISTER",
    payload: user,
  };
};
export const updateUser = (user: any) => {
  return {
    type: "UPDATE_USER",
    payload: user,
  };
};
export const logOut = () => {
  return {
    type: "LOG_OUT",
  };
};
