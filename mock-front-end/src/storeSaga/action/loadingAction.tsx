export const endLoading = () => {
  return {
    type: "END_LOADING",
  };
};
export const tagsLoading = () => {
  return {
    type: "LOADING_TAGS",
  };
};
export const endTagsLoading = () => {
  return {
    type: "END_LOADING_TAGS",
  };
};
export const endArticleLoading = () => {
  return {
    type: "END_LOADING_ARTICLE",
  };
};
export const endArticleProfileLoanding = () => {
  return {
    type: "END_LOADING_ARTICLE_PROFILES",
  };
};
export const articleProfileLoanding = () => {
  return {
    type: "LOADING_ARTICLE_PROFILES",
  };
};
export const loadingComment = () => {
  return {
    type: "LOADING_COMMENTS",
  };
};
export const endLoadingComment = () => {
  return {
    type: "END_LOADING_COMMENTS",
  };
};
