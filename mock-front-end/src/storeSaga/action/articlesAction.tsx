export const globalArticles = (params: any) => {
  return {
    type: "GLOBAL_ARTICLES",
    payload: params,
  };
};
export const yourFeed = (params: any) => {
  return {
    type: "YOUR_FEED",
    payload: params,
  };
};
