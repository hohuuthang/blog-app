import { takeEvery } from "redux-saga/effects";
import { fetchGlobalFeed, fetchYourFeed } from "./articleStore/articlesSaga";
import {
  fetchLogOut,
  fetchRegisterUser,
  fetchLoginUser,
  fetchUpdateUser,
} from "./userStore/userSaga";

function* rootSaga() {
  yield takeEvery("LOGIN", fetchLoginUser);
  yield takeEvery("REGISTER", fetchRegisterUser);
  yield takeEvery("UPDATE_USER", fetchUpdateUser)
  yield takeEvery("LOG_OUT", fetchLogOut);
  yield takeEvery("GLOBAL_ARTICLES", fetchGlobalFeed);
  yield takeEvery("YOUR_FEED", fetchYourFeed);
}
 
export default rootSaga;