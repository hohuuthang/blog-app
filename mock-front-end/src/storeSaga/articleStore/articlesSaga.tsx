import { SagaIterator } from "redux-saga";
import { PayloadAction } from "@reduxjs/toolkit";
import articleApi from "../../api/articleApi";
import { call, put } from "redux-saga/effects";
import { setArticle } from "./articlesSlice";

export function* fetchGlobalFeed(
  action: PayloadAction<string>
): SagaIterator<void> {
  try {
    const res = yield call(articleApi.getListArticle, action.payload);
    console.log("Global Fetch Article:", res);
    yield put({
      type: setArticle.type,
      payload: res,
    });
  } catch (error) {
    console.log(error);
  }
}
export function* fetchYourFeed(
  action: PayloadAction<string>
): SagaIterator<void> {
  try {
    const res = yield call(articleApi.feedArticle, action.payload);
    console.log("Your Fetch Article:", res);
    yield put({
      type: setArticle.type,
      payload: res,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* fetchFeedByFilter(
  action: PayloadAction<string>
): SagaIterator<void> {}
