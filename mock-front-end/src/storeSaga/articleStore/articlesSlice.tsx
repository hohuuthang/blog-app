import { RootState } from "../store";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  articles: [],
  articlesCount: 0,
};

const articlesSlice = createSlice({
  name: "article",
  initialState,
  reducers: {
    setArticle: (state, action) => {
      state.articles = action.payload.articles;
      state.articlesCount = action.payload.articlesCount;
    },
  },
});

export const { setArticle } = articlesSlice.actions;

export const selectArticles = (state: RootState) => state.articles.articles;
export const seleactArticlesCount = (state: RootState) =>
  state.articles.articlesCount;

export default articlesSlice.reducer;
