import { SagaIterator } from "redux-saga";
import { PayloadAction } from "@reduxjs/toolkit";
import { call, put } from "redux-saga/effects";
import userApi from "../../api/userApi";
import { saveUser, removeUser, errorUser } from "./userSlice";
import { toastSuccess } from "../../logic/functionList";

export function* fetchLoginUser(
  action: PayloadAction<string>
): SagaIterator<void> {
  try {
    const res = yield call(userApi.login, action.payload);
    console.log("Fetch Login: ", res);
    yield put({
      type: saveUser.type,
      payload: res,
    });
    toastSuccess("Login Success");
  } catch (error: any) {
    console.log(error.errors);
    yield put({
      type: errorUser.type,
      payload: error.errors,
    });
  }
}

export function* fetchRegisterUser(
  action: PayloadAction<string>
): SagaIterator<void> {
  try {
    const res = yield call(userApi.registration, action.payload);
    console.log("Fetch Registration: ", res);
    yield put({
      type: saveUser.type,
      payload: res,
    });
    toastSuccess("Register Success");
  } catch (error: any) {
    console.log(error);
    yield put({
      type: errorUser.type,
      payload: error.errors,
    });
  }
}

export function* fetchLogOut(): SagaIterator<void> {
  try {
    yield put({
      type: removeUser.type,
    });
  } catch (error) {
    console.log(error);
  }
}

export function* fetchUpdateUser(
  action: PayloadAction<string>
): SagaIterator<void> {
  try {
    const res = yield call(userApi.updateUser, action.payload);
    console.log("Fetch Update User: ", res);
    yield put({
      type: saveUser.type,
      payload: res,
    });
    toastSuccess("Update Success");
  } catch (error: any) {
    console.log(error);
    yield put({
      type: errorUser.type,
      payload: error.errors,
    });
  }
}
