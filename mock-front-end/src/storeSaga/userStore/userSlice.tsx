import { RootState } from "../store";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
  errorUser: null,
};
const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    saveUser: (state, action) => {
      localStorage.setItem("accessToken", action.payload.user.token);
      state.user = action.payload.user;
      state.errorUser = null;
    },
    removeUser: (state) => {
      state.user = null;
      localStorage.setItem("accessToken", "No Token");
    },
    errorUser: (state, action) => {
      console.log(action.payload);
      state.errorUser = action.payload;
    },
  },
});

export const { saveUser } = userSlice.actions;
export const { removeUser } = userSlice.actions;
export const { errorUser } = userSlice.actions;
export const selectUser = (state: RootState) => state.user.user;
export const selectErrorUser = (state: RootState) => state.user.errorUser;

export default userSlice.reducer;
