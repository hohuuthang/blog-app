import React from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Article from "./components/articleDetailsPage/Article";
import Home from "./components/homePage/Home";
import Newpost from "./components/NewPost";
import Profile from "./components/profilePage/Profile";
import Setting from "./components/Setting";
import Signin from "./components/Signin";
import Signup from "./components/Signup";
import Undefined from "./components/Undefined";
import Footer from "./components/commons/Footer";
import Header from "./components/commons/Header";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const App = () => {
  return (
    <div>
      <ToastContainer />
      <Header />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/newpost/:slug">
          <Newpost />
        </Route>
        <Route path="/newpost">
          <Newpost />
        </Route>
        <Route path="/setting">
          <Setting />
        </Route>
        <Route path="/signup">
          <Signup />
        </Route>
        <Route path="/signin">
          <Signin />
        </Route>
        <Route path="/profile/:username">
          <Profile />
        </Route>
        <Route path="/article/:slug">
          <Article />
        </Route>
        <Route path="*">
          <Undefined />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
};

export default App;
