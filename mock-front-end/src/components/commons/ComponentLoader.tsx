/* eslint-disable jsx-a11y/alt-text */
import LoadingComponent from "../../public/images/LoadingComponent.gif";

const ComponentLoader = () => {
  return (
    <div className="loader-component-container">
      <div className="loader-loader-component">
        <img src={LoadingComponent} width="100px" />
      </div>
    </div>
  );
};
export default ComponentLoader;
