/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { NavLink } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../storeSaga/hook";
import { selectUser } from "../../storeSaga/userStore/userSlice";
import { useHistory } from "react-router";
import { logOut } from "../../storeSaga/action/userActioncs";
import USER from "../interface/user";
const Header = () => {
  const history = useHistory();
  const dispatch = useAppDispatch();
  const currentUser: USER | any = useAppSelector(selectUser);
  const handleLogOut = () => {
    let action = logOut();
    dispatch(action);
    history.push("/");
  };
  return (
    <div>
      <nav className="navbar navbar-light">
        <div className="container">
          <NavLink to="/" className="navbar-brand">
            conduit
          </NavLink>
          {currentUser !== null ? (
            <ul className="nav navbar-nav pull-xs-right">
              <li className="nav-item">
                <NavLink to="/" className="nav-link active">
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/newpost" className="nav-link">
                  <i className="ion-compose"></i>&nbsp;New Post
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/setting" className="nav-link">
                  <i className="ion-gear-a"></i>&nbsp;Settings
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to={"/profile/" + currentUser.username}
                  className="nav-link"
                >
                  Profile of {currentUser.username}
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" onClick={handleLogOut}>
                  Log Out
                </a>
              </li>
            </ul>
          ) : (
            <ul className="nav navbar-nav pull-xs-right">
              <li className="nav-item">
                <NavLink to="/" className="nav-link active">
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/signup" className="nav-link">
                  Sign up
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/signin" className="nav-link">
                  Sign in
                </NavLink>
              </li>
            </ul>
          )}
        </div>
      </nav>
    </div>
  );
};

export default Header;
