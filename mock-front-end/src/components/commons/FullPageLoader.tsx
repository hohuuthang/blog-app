/* eslint-disable jsx-a11y/alt-text */
import LoaderFullPageGif from "../../public/images/LoadingFullPage.gif";
import { useAppSelector } from "../../storeSaga/hook";
import { RootState } from "../../storeSaga/store";

const FullPageLoader = (props: any) => {
  const loading = useAppSelector(
    (state: RootState) => state.loading.loadingPage
  );
  console.log(loading);
  if (!loading) return null;
  return (
    <div className="loader-container">
      <div className="loader">
        <img src={LoaderFullPageGif} width="100px" />
      </div>
    </div>
  );
};
export default FullPageLoader;
