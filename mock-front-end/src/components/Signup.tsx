/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useAppDispatch, useAppSelector } from "../storeSaga/hook";
import { useHistory } from "react-router";
import { selectErrorUser, selectUser } from "../storeSaga/userStore/userSlice";
import FullPageLoader from "./commons/FullPageLoader";
import { endLoading } from "../storeSaga/action/loadingAction";
import { registerUser } from "../storeSaga/action/userActioncs";

const Signup = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const error = useAppSelector(selectErrorUser);
  const currentUser = useAppSelector(selectUser);
  if (error != null) {
    dispatch(endLoading());
  }
  useEffect(() => {
    dispatch(endLoading());
    if (currentUser != null) history.push("/");
  }, [currentUser]);
  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign Up</h1>
            <p id="hide" className="text-xs-center"></p>
            <Formik
              initialValues={{ username: "", email: "", password: "" }}
              validate={(values) => {
                const errors: any = {};
                if (!values.email) {
                  errors.email = "Required";
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = "Invalid format of email address";
                }
                if (!values.password) {
                  errors.password = "Required";
                }
                if (!values.username) {
                  errors.username = "Required";
                }
                return errors;
              }}
              onSubmit={(values) => {
                let user = {
                  username: values.username,
                  email: values.email,
                  password: values.password,
                };
                let action = registerUser(user);
                dispatch(action);
              }}
            >
              {({ isValid }) => (
                <Form>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="text"
                      name="username"
                      placeholder="User Name"
                    />
                    <ErrorMessage
                      name="username"
                      component="div"
                      className="messageError"
                    />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="email"
                      name="email"
                      placeholder="Email"
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="messageError"
                    />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="password"
                      name="password"
                      placeholder="Password"
                    />
                    <ErrorMessage
                      name="password"
                      component="div"
                      className="messageError"
                    />
                  </fieldset>
                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={!isValid}
                  >
                    Sign Up
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
      <FullPageLoader></FullPageLoader>
    </div>
  );
};

export default Signup;
