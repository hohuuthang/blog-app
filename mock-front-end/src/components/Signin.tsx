/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useAppDispatch, useAppSelector } from "../storeSaga/hook";
import { useHistory } from "react-router";
import { selectErrorUser, selectUser } from "../storeSaga/userStore/userSlice";
import FullPageLoader from "./commons/FullPageLoader";
import { endLoading } from "../storeSaga/action/loadingAction";
import { loginUser } from "../storeSaga/action/userActioncs";

const SignIn = () => {
  const dispatch = useAppDispatch();
  const currentUser = useAppSelector(selectUser);
  const error = useAppSelector(selectErrorUser);
  const history = useHistory();
  if (error != null) {
    dispatch(endLoading());
  }
  useEffect(() => {
    dispatch(endLoading());
    if (currentUser != null) history.push("/");
  }, [currentUser]);
  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign In</h1>
            <p className="text-xs-center"></p>
            <Formik
              initialValues={{ email: "", password: "" }}
              validate={(values) => {
                const errors: any = {};
                if (!values.email) {
                  errors.email = "Required";
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = "Invalid format of email address";
                }
                if (!values.password) {
                  errors.password = "Required";
                }
                return errors;
              }}
              onSubmit={(values) => {
                let user = {
                  email: values.email,
                  password: values.password,
                };
                let action = loginUser(user);
                dispatch(action);
              }}
            >
              {({ isValid }) => (
                <Form>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="email"
                      name="email"
                      placeholder="Email"
                    />
                    <ErrorMessage
                      className="messageError"
                      name="email"
                      component="div"
                    />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="password"
                      name="password"
                      placeholder="Password"
                    />
                    <ErrorMessage
                      className="messageError"
                      name="password"
                      component="div"
                    />
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={!isValid}
                  >
                    Sign In
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
      <FullPageLoader></FullPageLoader>
    </div>
  );
};

export default SignIn;
