/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable eqeqeq */
import React, { useEffect, useState } from "react";
import { Formik, ErrorMessage, Field, Form, FieldArray } from "formik";
import { useHistory, useParams } from "react-router";
import articleApi from "../api/articleApi";
import { useAppSelector } from "../storeSaga/hook";
import { selectUser } from "../storeSaga/userStore/userSlice";
import { toastSuccess } from "../logic/functionList";
import NewArticle from "./interface/article";
import USER from "./interface/user";


const initialValues = {
  title: "",
  body: "",
  description: "",
  tagList: [],
};

const Newpost = () => {
  const history = useHistory();
  const slug: string = useParams<any>().slug;
  const currentUser: USER | any = useAppSelector(selectUser);
  const [articleDetail, setArticleDetail] = useState<NewArticle>();
  useEffect(() => {
    const getArticle = async () => {
      let article: NewArticle | any = await articleApi.getOneArticle(slug);
      let obj: NewArticle = {
        title: article.article.title,
        body: article.article.body,
        description: article.article.description,
        tagList: article.article.tagList,
      };
      setArticleDetail(obj);
    };
    if (slug !== undefined) getArticle();
  }, []);
  const handleSubmit = (values: NewArticle) => {
    console.log(values);
    const actionArticle = async (funcApi: Function) => {
      await funcApi(values, slug);
    };
    var message = "";
    if (slug === undefined) {
      actionArticle(articleApi.createArticle);
      message = "Create Article";
    }
    else {
      actionArticle(articleApi.updateArticle);
      message = "Update Article";
    }
    toastSuccess(message + " Successfully");
    history.push("/profile/" + currentUser.username);
  };
  console.log(articleDetail);
  return (
    <div>
      <Formik
        enableReinitialize={true}
        onSubmit={handleSubmit}
        initialValues={
          articleDetail != undefined ? articleDetail : initialValues
        }
        validate={(values) => {
          const error: any = {};
          if (!values.title) error.title = "Required!";
          if (!values.body) error.body = "Required!";
          if (!values.description) error.description = "Required!";
          return error;
        }}
        render={({ values }) => (
          <Form>
            <div className="container">
              <div className="row">
                <div className="col-md-10 offset-md-1 col-xs-12">
                  <div>
                    <label htmlFor="title"></label>
                    <Field
                      name="title"
                      className="form-control"
                      id="name"
                      placeholder="Article Title"
                    />
                    <ErrorMessage
                      component="div"
                      className="alert alert-danger errorNewPost"
                      name="title"
                    />
                  </div>
                  <div>
                    <label htmlFor="description"></label>
                    <Field
                      name="description"
                      className="form-control"
                      row="5"
                      id="description"
                      placeholder="What's this article about?"
                    />
                    <ErrorMessage
                      component="div"
                      className="alert alert-danger errorNewPost"
                      name="description"
                    />
                  </div>
                  <div>
                    <label htmlFor="body"></label>
                    
                    <Field
                      as="textarea"
                      name="body"
                      className="form-control"
                      id="body"
                      placeholder="Write your article(in markdown)"
                    />
                    <ErrorMessage
                      component="div"
                      className="alert alert-danger errorNewPost"
                      name="body"
                    />
                  </div>
                  <FieldArray
                    name="tagList"
                    render={(arrayHelpers: any) => (
                      <div className="tagNewPost">
                        {values.tagList && values.tagList.length > 0
                          ? values.tagList.map((tag: any, index: any) => (
                              <div className="form-row" key={index}>
                                <div className="form-group col-md-6">
                                  <Field
                                    className="form-control"
                                    name={`tagList.${index}`}
                                    placeholder="Tag"
                                    type="text"
                                    required
                                  />
                                </div>
                                <div className="form-group col-md-6">
                                  <button
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={() => arrayHelpers.remove(index)}
                                  >
                                    X
                                  </button>
                                </div>
                              </div>
                            ))
                          : null}
                        <hr />
                        <button
                          type="button"
                          className="btn btn-success"
                          onClick={() => arrayHelpers.push("")}
                        >
                          Add Tag
                        </button>
                      </div>
                    )}
                  />
                  <button
                    className="btn btn-lg pull-xs-right btn-primary"
                    type="submit"
                  >
                    Public Article
                  </button>
                </div>
              </div>
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default Newpost;
