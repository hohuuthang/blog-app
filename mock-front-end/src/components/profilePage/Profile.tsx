/* eslint-disable react/jsx-pascal-case */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import userApi from "../../api/userApi";
import My_articles from "./ArticlesOfProfiles";
import { useParams } from "react-router";
import articleApi from "../../api/articleApi";
import { useAppSelector } from "../../storeSaga/hook";
import { selectUser } from "../../storeSaga/userStore/userSlice";
import { useAppDispatch } from "../../storeSaga/hook";
import {
  articleProfileLoanding,
  endArticleProfileLoanding,
} from "../../storeSaga/action/loadingAction";
import Paging from "../homePage/Paging";
import { caculateNumOfPage } from "../../logic/functionList";
import USER from "../interface/user";

const Profile = () => {
  const currentUser: USER | any = useAppSelector(selectUser);
  const username: string = useParams<any>().username;
  let dispatch = useAppDispatch();
  const size = 3;
  const [userdata, setUserdata] = useState<any>({});
  const [listArticle, setListArticle] = useState<any>([]);
  const [active, setActive] = useState(true);
  const [activePage, setActivePage] = useState<any>(1);
  const [numOfPage, setNumOfPage] = useState<any>(0);
  const [typeGetArticle, setTypeGetArticle] = useState("");
  useEffect(() => {
    const getUserData = async () => {
      let action = articleProfileLoanding();
      dispatch(action);
      const userProfile: any = await userApi.getProfileByUserName(username);
      const list: any = await articleApi.getListArticle({
        author: username,
        limit: size,
      });
      action = endArticleProfileLoanding();
      dispatch(action);
      setUserdata(userProfile.profile);
      setListArticle(list.articles);
      let tempNum = caculateNumOfPage(list.articlesCount, size);
      setNumOfPage(tempNum);
    };
    getUserData();
  }, [username]);
  const getArticlesByType = async (typeGet: string) => {
    setTypeGetArticle(typeGet);
    let params = {
      [typeGet]: username,
      limit: size,
    };
    let action = articleProfileLoanding();
    dispatch(action);
    const list: any = await articleApi.getListArticle(params);
    action = endArticleProfileLoanding();
    dispatch(action);
    setListArticle(list.articles);
    setActivePage(1);
    setNumOfPage(caculateNumOfPage(list.articlesCount, size));
    console.log(list);
    
  };
  const handleChangeArticles = (e: any) => {
    if (active === true) setActive(false);
    else setActive(true);
    getArticlesByType(e.target.id);
    e.preventDefault();
  };
  const handleFollow = (e: any) => {
    const setFollow = async () => {
      if (e.target.id === "true") {
        let res: any = await userApi.unfollowUserByUserName(username);
        setUserdata(res.profile);
      } else {
        let res: any = await userApi.followUserByUserName(username);
        setUserdata(res.profile);
      }
    };
    setFollow();
  };
  const handleChangePage = (pageIndex: any) => {
    let params: any = {
      [typeGetArticle]: username,
      limit: size,
      offset: (pageIndex - 1) * size,
    };
    const getArticleOfPageIndex = async (params: any) => {
      let action = articleProfileLoanding();
      dispatch(action);
      let list: any = await articleApi.getListArticle(params);
      action = endArticleProfileLoanding();
      dispatch(action);
      setListArticle(list.articles);
    };

    setActivePage(pageIndex);
    getArticleOfPageIndex(params);
  };
  return (
    <div className="profile-page">
      <div className="user-info">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-10 offset-md-1">
              <img src={userdata.image} className="user-img" />
              <h4>{userdata.username}</h4>
              <p>{userdata.bio}</p>
              <button
                id={"" + userdata.following}
                className="btn btn-sm btn-outline-success action-btn"
                onClick={handleFollow}
                disabled={
                  currentUser === null
                    ? true
                    : currentUser.username === username
                    ? true
                    : false
                }
              >
                {userdata.following === true ? (
                  <i className="ion-checkmark-round"></i>
                ) : (
                  <i className="ion-plus-round"></i>
                )}
                &nbsp; Follow {userdata.username}
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-10 offset-md-1">
            <div className="articles-toggle">
              <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                  <a
                    className={`nav-link ${active ? "active" : ""}`}
                    href=""
                    onClick={handleChangeArticles}
                    id="author"
                  >
                    My Articles
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={`nav-link ${active ? "" : "active"}`}
                    href=""
                    onClick={handleChangeArticles}
                    id="favorited"
                  >
                    Favorited Articles
                  </a>
                </li>
              </ul>
            </div>
            <div className="article-preview">
              <My_articles
                listArticle={listArticle}
                currentUser={currentUser}
              ></My_articles>
            </div>
            <Paging
              activePage={activePage}
              numOfPage={numOfPage}
              changePage={handleChangePage}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
