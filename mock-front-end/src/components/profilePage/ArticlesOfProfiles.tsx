/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { toDate } from "../../logic/functionList";
import { useHistory } from "react-router";
import { useAppSelector } from "../../storeSaga/hook";
import { RootState } from "../../storeSaga/store";
import ComponentLoader from "../commons/ComponentLoader";
import article from "../interface/article";
import { SyntheticEvent } from "react";

const My_articles = (props: any) => {
  let listArtiles = props.listArticle;
  let currentUser = props.currentUser;
  const loading = useAppSelector(
    (state: RootState) => state.loading.loadingArticlesProfile
  );
  const history = useHistory();
  const handleMoveToArticleDetail = (author: Object, slug: string, event: SyntheticEvent) => {
    history.push({
      pathname: "/article/" + slug,
      state: { author: author },
    });
    event.preventDefault();
  };
  const handleEditArticle = (slug: string, event: SyntheticEvent) => {
    history.push("/newpost/" + slug);
    event.preventDefault();
  };
  return (
    <div>
      {loading ? (
        <ComponentLoader></ComponentLoader>
      ) : (
        <div>
          {listArtiles.map((article: article | any, index: any) => {
            return (
              <div key={index}>
                <div className="article-meta">
                  <a href="" id={article.author.username}>
                    <img src={article.author.image} />
                  </a>
                  <div className="info">
                    <a href="" className="author" id={article.author.username}>
                      {article.author.username}
                    </a>
                    <span className="date">{toDate(article.createdAt)}</span>
                  </div>
                  <button
                    className={`btn btn-outline-primary btn-sm pull-xs-right ${
                      article.favorited === true ? "active" : ""
                    }`}
                  >
                    <i className="ion-heart"></i> {article.favoritesCount}
                  </button>
                </div>
                <h1>{article.title}</h1>
                <p>{article.description}</p>
                <div>
                  {article.tagList.map((tag: any, index: any) => {
                    return (
                      <span
                        key={index}
                        className="tag-pill tag-default"
                        id={tag}
                      >
                        {tag}
                      </span>
                    );
                  })}
                </div>
                <br />
                <button
                  className="btn btn-success btn-sm"
                  onClick={(event: SyntheticEvent) =>
                    handleMoveToArticleDetail(
                      article.author.username,
                      article.slug,
                      event
                    )
                  }
                  id={article.slug}
                >
                  Read more...
                </button>
                {currentUser === null ? (
                  <div></div>
                ) : currentUser.username === article.author.username ? (
                  <button
                    className="btn btn-success btn-sm edit"
                    onClick={(event: SyntheticEvent) =>
                      handleEditArticle(article.slug, event)
                    }
                  >
                    Edit
                  </button>
                ) : (
                  <div></div>
                )}
                <hr />
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default My_articles;
