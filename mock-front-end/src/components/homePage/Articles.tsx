/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { useHistory } from "react-router";
import { toDate } from "../../logic/functionList";
import articleApi from "../../api/articleApi";
import { useAppSelector } from "../../storeSaga/hook";
import { RootState } from "../../storeSaga/store";
import ComponentLoader from "../commons/ComponentLoader";
import article from "../interface/article";
import { SyntheticEvent } from "react";

const GetArticles = (props: any) => {
  const listArtiles: article | any = props.articles;
  const activePage: any = props.activePage;
  const loading = useAppSelector(
    (state: RootState) => state.loading.loadingArticles
  );
  const history = useHistory();
  const handleMoveToProfile = (authorName: string, event: SyntheticEvent) => {
    history.push("/profile/" + authorName);
    event.preventDefault();
  };
  const handleMoveToArticleDetail = (author: string, slug: string, event: SyntheticEvent) => {
    history.push({
      pathname: "/article/" + slug,
      state: { author: author },
    });
    event.preventDefault();
  };
  const handleClick = (e: any) => {
    console.log();
    props.filterValue(e.target.id);
    e.preventDefault();
  };
  return (
    <div>
      {loading ? (
        <ComponentLoader></ComponentLoader>
      ) : (
        <div>
          {listArtiles.map((article: article|any, index: any) => {
            const handleChangeFavorites = (event: SyntheticEvent) => {
              let token = localStorage.getItem("accessToken");
              const fetchFavorite = async (funcApi: Function) => {
                await funcApi(article.slug);
                props.keepPage(activePage);
              };
              if (token !== "No Token") {
                if (article.favorited) {
                  fetchFavorite(articleApi.unfavoriteArticle);
                } else {
                  fetchFavorite(articleApi.favoriteArticle);
                }
              }
              event.preventDefault();
            };
            return (
              <div key={index}>
                <div className="article-meta">
                  <a
                    href=""
                    onClick={(e: SyntheticEvent) =>
                      handleMoveToProfile(article.author.username, e)
                    }
                  >
                    <img src={article.author.image} />
                  </a>
                  <div className="info">
                    <a
                      href=""
                      className="author"
                      onClick={(e: SyntheticEvent) =>
                        handleMoveToProfile(article.author.username, e)
                      }
                    >
                      {article.author.username}
                    </a>
                    <span className="date">{toDate(article.createdAt)}</span>
                  </div>
                  <button
                    className={`btn btn-outline-primary btn-sm pull-xs-right ${
                      article.favorited === true ? "active" : ""
                    }`}
                    onClick={handleChangeFavorites}
                  >
                    <i className="ion-heart"></i> {article.favoritesCount}
                  </button>
                </div>
                <h1>{article.title}</h1>
                <p>{article.description}</p>
                <div>
                  {article.tagList.map((tag: any, index: any) => {
                    return (
                      <a
                        onClick={handleClick}
                        key={index}
                        href=""
                        className="tag-pill tag-default"
                        id={tag}
                      >
                        {tag}
                      </a>
                    );
                  })}
                </div>
                <br />
                <button
                  className="btn btn-success btn-sm"
                  onClick={(e) =>
                    handleMoveToArticleDetail(
                      article.author.username,
                      article.slug,
                      e
                    )
                  }
                >
                  Read more...
                </button>
                <hr />
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default GetArticles;
