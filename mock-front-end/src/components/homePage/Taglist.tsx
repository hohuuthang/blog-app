/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import tagApi from "../../api/tagApi";
import { useEffect, useState } from "react";
import { useAppDispatch } from "../../storeSaga/hook";
import ComponentLoader from "../commons/ComponentLoader";
import {
  endTagsLoading,
  tagsLoading,
} from "../../storeSaga/action/loadingAction";
import { useAppSelector } from "../../storeSaga/hook";
import { RootState } from "../../storeSaga/store";

const GetTaglist = (props: any) => {
  const loading = useAppSelector(
    (state: RootState) => state.loading.loadingTags
  );
  const [tags, setTags] = useState([]);
  const dispatch = useAppDispatch();
  let action: any = {};
  useEffect(() => {
    const getTaglist = async () => {
      action = tagsLoading();
      await dispatch(action);
      let listtag: any = await tagApi.getListOfTags();
      action = endTagsLoading();
      await dispatch(action);
      setTags(listtag.tags);
    };
    getTaglist();
  }, []);
  const handleClick = (e: any) => {
    props.filterValue(e.target.id);
    e.preventDefault();
  };
  return (
    <div>
      <div className="sidebar">
        <p>Popular Tags</p>
        {loading ? (
          <ComponentLoader />
        ) : (
          <div className="tag-list">
            {tags.map((tag, index) => {
              return (
                <a
                  onClick={handleClick}
                  key={index}
                  href=""
                  className="tag-pill tag-default"
                  id={tag}
                >
                  {tag}
                </a>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default GetTaglist;
