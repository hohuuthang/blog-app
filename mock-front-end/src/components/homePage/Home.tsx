/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import GetArticles from "./Articles";
import GetTaglist from "./Taglist";
import { useState } from "react";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../storeSaga/hook";
import {
  selectArticles,
  seleactArticlesCount,
} from "../../storeSaga/articleStore/articlesSlice";
import Paging from "./Paging";
import { caculateNumOfPage } from "../../logic/functionList";
import { selectUser } from "../../storeSaga/userStore/userSlice";
import {
  globalArticles,
  yourFeed,
} from "../../storeSaga/action/articlesAction";
import { endArticleLoading } from "../../storeSaga/action/loadingAction";
import { toast } from "react-toastify";
import article from "../interface/article";
import USER from "../interface/user";

const Home = () => {
  const dispatch = useAppDispatch();
  let articles: article | any = useAppSelector(selectArticles);
  let articlesCount: number = useAppSelector(seleactArticlesCount);
  let currentUser: USER | any = useAppSelector(selectUser);
  if (currentUser === null) {
    localStorage.setItem("accessToken", "No Token");
  }
  const size = 4;
  const [activePage, setActivePage] = useState(1);
  const [currentTag, setCurrentTag] = useState("");
  const [activeGlobal, setActiveGlobal] = useState("active");
  const [activeFeed, setActiveFeed] = useState("");
  useEffect(() => {
    let actionLoading = endArticleLoading();
    dispatch(actionLoading);
  }, [articles]);
  useEffect(() => {
    let action = globalArticles({ limit: size });
    dispatch(action);
  }, [currentUser]);
  const handleChangePage = (pageIndex: number) => {
    let payload: any = {
      limit: size,
      offset: (pageIndex - 1) * size,
    };
    if (currentTag !== "") {
      payload.tag = currentTag;
    }
    let action: any = {};
    if (activeFeed !== "") {
      action = yourFeed(payload);
    } else {
      action = globalArticles(payload);
    }
    setActivePage(pageIndex);
    dispatch(action);
  };
  const handleFilter = (value: any) => {
    setCurrentTag(value);
    setActiveGlobal("active");
    setActiveFeed("");
    let action = globalArticles({
      tag: value,
      limit: size,
    });
    dispatch(action);
  };
  const handleChangeType = (e: any) => {
    setCurrentTag("");
    let action = globalArticles({
      limit: size,
    });
    if (e.target.id === "global") {
      setActiveGlobal("active");
      setActiveFeed("");
    } else {
      if (currentUser !== null) {
        setActiveGlobal("");
        setActiveFeed("active");
        action = yourFeed({
          limit: size,
        });
      } else {
        toast.error("Please Login To See Your Feed");
      }
    }
    dispatch(action);
    e.preventDefault();
  };
  let numOfPage = caculateNumOfPage(articlesCount, size);
  return (
    <div className="home-page">
      <div className="banner">
        <div className="container">
          <h1 className="logo-font">conduit</h1>
          <p>A place to share your knowledge.</p>
        </div>
      </div>

      <div className="container page">
        <div className="row">
          <div className="col-md-9">
            <div className="feed-toggle">
              <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                  <a
                    className={`nav-link ${activeFeed}`}
                    onClick={handleChangeType}
                    id="youFeed"
                  >
                    Your Feed
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={`nav-link ${activeGlobal}`}
                    onClick={handleChangeType}
                    id="global"
                  >
                    Global Feed
                  </a>
                </li>
              </ul>
            </div>

            <div className="article-preview">
              <GetArticles
                filterValue={handleFilter}
                articles={articles}
                keepPage={handleChangePage}
                activePage={activePage}
              />
            </div>
          </div>

          <div className="col-md-3">
            <GetTaglist filterValue={handleFilter} />
          </div>
        </div>
        <div>
          <Paging
            activePage={activePage}
            numOfPage={numOfPage}
            changePage={handleChangePage}
          />
        </div>
      </div>
    </div>
  );
};

export default Home;
