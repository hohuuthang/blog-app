import React from "react";
import Pagination from 'react-bootstrap/Pagination';
 
const Paging = (props: any) => {
    let active : number = Number(props.activePage);
    let items = [];
    const handleMovePage = (event:any) =>{
      props.changePage(event.target.getAttribute("value"));
      event.preventDefault();
    }
    for (let number = 1; number <= props.numOfPage; number++) {
      items.push(
          <Pagination.Item key={number} active={number === active} onClick ={handleMovePage} value={number}>
          {number}
          </Pagination.Item>,
      );
    }
    return (
          <Pagination>{items}</Pagination>
    )
}
export default Paging;