/* eslint-disable eqeqeq */
/* eslint-disable jsx-a11y/alt-text */
import { useAppDispatch, useAppSelector } from "../storeSaga/hook";
import { selectUser } from "../storeSaga/userStore/userSlice";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { useHistory } from "react-router";
import { updateUser } from "../storeSaga/action/userActioncs";
import USER from "./interface/user";
const Setting = () => {
  let currentUser: USER | any = useAppSelector(selectUser);
  const dispatch = useAppDispatch();
  const history = useHistory();
  return (
    <div className="settings-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center imgSetting">Your Settings</h1>
            <Formik
              initialValues={{
                imageURL: currentUser.image,
                username: currentUser.username,
                bio: currentUser.bio,
                email: currentUser.email,
                password: "",
              }}
              validate={(values) => {
                const errors: any = {};
                if (!values.email) {
                  errors.email = "Required";
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = "Invalid format of email address";
                }
                if (!values.username) {
                  errors.username = "Required";
                }
                return errors;
              }}
              onSubmit={(values) => {
                let user: USER| any = {
                  image: values.imageURL,
                  email: values.email,
                  username: values.username,
                  bio: values.bio,
                };
                if (values.password != "") {
                  user.password = values.password;
                }
                let action = updateUser(user);
                dispatch(action);
                history.push("/");
              }}
            >
              {({ values, isValid }) => (
                <Form>
                  <fieldset className="form-group">
                    <Field
                      className="form-control"
                      type="text"
                      name="imageURL"
                      placeholder="URL of profile picture"
                    />
                    <ErrorMessage name="imageURL" component="div" />
                  </fieldset>
                  {values.imageURL !== "" ? (
                    <fieldset className="form-group imgSetting">
                      <img src={values.imageURL} width="50%" />
                    </fieldset>
                  ) : (
                    <div></div>
                  )}
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="text"
                      name="username"
                      placeholder="User Name"
                    />
                    <ErrorMessage name="username" component="div" />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      as="textarea"
                      className="form-control form-control-lg"
                      type="text"
                      name="bio"
                      placeholder="Short bio about you"
                    />
                    <ErrorMessage name="bio" component="div" />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="email"
                      name="email"
                      placeholder="Email"
                    />
                    <ErrorMessage name="email" component="div" />
                  </fieldset>
                  <fieldset className="form-group">
                    <Field
                      className="form-control form-control-lg"
                      type="password"
                      name="password"
                      placeholder="Password"
                    />
                    <ErrorMessage name="password" component="div" />
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={!isValid}
                  >
                    Update
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Setting;
