/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { selectUser } from "../../storeSaga/userStore/userSlice";
import { useAppSelector } from "../../storeSaga/hook";
import {
  caculateNumOfPage,
  getListCommentPaging,
  toDate,
} from "../../logic/functionList";
import { RootState } from "../../storeSaga/store";
import ComponentLoader from "../commons/ComponentLoader";
import { useEffect, useState } from "react";
import Paging from "../homePage/Paging";
import comment from "../interface/comment";
import USER from "../interface/user";

const Comment = (props: any) => {
  const commentData: comment | any = props.comments;
  const [activePage, setActivePage] = useState<any>(1);
  const loading = useAppSelector(
    (state: RootState) => state.loading.loadingComments
  );
  const currentUser: USER | any = useAppSelector(selectUser);
  const size = 3;
  const [commentDisplay, setCommentDisplay] = useState<any>([]);
  const numOfPage = caculateNumOfPage(commentData.length, size);
  const handleDeleteComment = (id: string, e: any) => {
    props.onDeleteComment(id);
    e.preventDefault();
  };
  const handleChangePage = (pageIndex: any) => {
    let tempArray = getListCommentPaging(commentData, pageIndex, size);
    setActivePage(pageIndex);
    setCommentDisplay(tempArray);
  };
  useEffect(() => {
    setCommentDisplay(getListCommentPaging(commentData, 1, size));
  }, [commentData]);
  return (
    <div>
      {loading ? (
        <ComponentLoader></ComponentLoader>
      ) : (
        <div>
          {commentDisplay.map((comment: comment, index: any) => {
            return (
              <div className="card" key={index}>
                <div className="card-block">
                  <p className="card-text">{comment.body}</p>
                </div>
                <div className="card-footer">
                  <a href="" className="comment-author">
                    <img
                      src={comment.author.image}
                      className="comment-author-img"
                    />
                  </a>
                  &nbsp;
                  <a href="" className="comment-author">
                    {comment.author.username}
                  </a>
                  <span className="date-posted">
                    {toDate(comment.createdAt)}
                  </span>
                  {currentUser === null ? null : currentUser.username ===
                    comment.author.username ? (
                    <button
                      className="btn btn-sm btn-danger deleteComment"
                      onClick={(e) => handleDeleteComment(comment.id, e)}
                    >
                      <i className="ion-trash-a">&nbsp;Delete Commment</i>
                    </button>
                  ) : null}
                </div>
              </div>
            );
          })}
          <Paging
            activePage={activePage}
            numOfPage={numOfPage}
            changePage={handleChangePage}
          />
        </div>
      )}
    </div>
  );
};

export default Comment;
