/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { SyntheticEvent, useEffect, useRef, useState } from "react";
import { useHistory, useLocation } from "react-router";
import commentApi from "../../api/commentApi";
import userApi from "../../api/userApi";
import Comment from "./Comment";
import { useParams } from "react-router";
import articleApi from "../../api/articleApi";
import { useAppDispatch, useAppSelector } from "../../storeSaga/hook";
import { selectUser } from "../../storeSaga/userStore/userSlice";
import { toDate } from "../../logic/functionList";
import {
  endLoadingComment,
  loadingComment,
} from "../../storeSaga/action/loadingAction";
import USER from "../interface/user";
import comment from "../interface/comment";
import article from "../interface/article";

const Article = () => {
  const dispatch = useAppDispatch();
  const currentUser: USER | any = useAppSelector(selectUser);
  const history = useHistory();
  const location: any = useLocation();
  const slug: string = useParams<any>().slug;
  const authorOfArticle: any = location.state.author;
  const [userData, setUserData] = useState<any>({});
  const [articleData, setArticleData] = useState<any>({});
  const [comments, setComments] = useState<any>([]);
  const commentData: comment | any = useRef(null);
  const handleAddComment = (e: SyntheticEvent) => {
    const addComment = async () => {
      let action = loadingComment();
      dispatch(action);
      await commentApi.addCommentToArticle(
        {
          body: commentData.current.value,
        },
        slug
      );
      let listComment: comment[] | any = await commentApi.getCommentFromArticle(slug);
      action = endLoadingComment();
      dispatch(action);
      setComments(listComment.comments);
    };
    addComment();
    commentData.current.value = "";
    e.preventDefault();
  };
  const handleDeleteComment = (id: string) => {
    const deleteComment = async () => {
      let action = loadingComment();
      dispatch(action);
      await commentApi.deleteComment(slug, id);
      let listComment: comment[] | any = await commentApi.getCommentFromArticle(slug);
      action = endLoadingComment();
      dispatch(action);
      setComments(listComment.comments);
    };

    deleteComment();
  };
  const handleDeleteArticle = (slug: string, e: SyntheticEvent) => {
    const deleteArticle = async () => {
      await articleApi.deleteArticle(slug);
    };
    deleteArticle();
    history.push("/profile/" + currentUser.username);
    e.preventDefault();
  };
  const handleMoveToProfile = (authorName: string, event: SyntheticEvent) => {
    history.push("/profile/" + authorName);
    event.preventDefault();
  };
  useEffect(() => {
    const getDataOfPage = async () => {
      let action = loadingComment();
      dispatch(action);
      let profile: any = await userApi.getProfileByUserName(authorOfArticle);
      let article: article | any = await articleApi.getOneArticle(slug);
      let listComment: comment[] | any = await commentApi.getCommentFromArticle(slug);
      action = endLoadingComment();
      dispatch(action);
      setComments(listComment.comments);
      setUserData(profile.profile);
      setArticleData(article.article);
    };
    getDataOfPage();
  }, []);
  return (
    <div className="article-page">
      <div className="banner">
        <div className="container">
          <h1>{userData.bio}</h1>
          <div className="article-meta">
            <a
              href=""
              onClick={(e: SyntheticEvent) => handleMoveToProfile(userData.username, e)}
            >
              <img src={userData.image} />
            </a>
            <div className="info">
              <a
                href=""
                className="author"
                onClick={(e: SyntheticEvent) => handleMoveToProfile(userData.username, e)}
              >
                {userData.username}
              </a>
              <span className="date">{toDate(articleData.createdAt)}</span>
            </div>
            {currentUser === null ? (
              <div></div>
            ) : currentUser.username === authorOfArticle ? (
              <button
                className="btn btn-sm btn-outline-secondary"
                onClick={(e) => handleDeleteArticle(articleData.slug, e)}
              >
                <i className="ion-trash-a">&nbsp;Delete Article</i>
              </button>
            ) : (
              <div></div>
            )}
            &nbsp;&nbsp;
          </div>
        </div>
      </div>

      <div className="container page">
        <div className="row article-content">
          <div className="col-md-12">
            <p>{articleData.description}</p>
            <h2 id="introducing-ionic">{articleData.title}</h2>
            <p>{articleData.body}</p>
          </div>
        </div>

        <hr />
        {currentUser !== null ? (
          <div>
            <div className="article-actions">
              <div className="article-meta">
                <a href="profile.html">
                  <img src={currentUser.image} />
                </a>
                <div className="info">
                  <a href="" className="author">
                    {currentUser.username}
                  </a>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-xs-12 col-md-8 offset-md-2">
                <form className="card comment-form">
                  <div className="card-block">
                    <textarea
                      className="form-control"
                      placeholder="Write a comment..."
                      ref={commentData}
                    ></textarea>
                  </div>
                  <div className="card-footer">
                    <img
                      src={currentUser.image}
                      className="comment-author-img"
                    />
                    <button
                      onClick={handleAddComment}
                      className="btn btn-sm btn-primary"
                    >
                      Post Comment
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
        <div className="row">
          <div className="col-xs-12 col-md-8 offset-md-2">
            <Comment
              comments={comments}
              onDeleteComment={handleDeleteComment}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Article;
