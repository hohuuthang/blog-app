/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import styled from 'styled-components'

const DIV404 = styled.div`
    margin: auto;
    width: 83vw;
    height: 83vh;
    img{
        width: 100%;
        height: 100%;
    }
`
const Undefined = () => {
    return (
        <DIV404>
            <img src="https://miro.medium.com/max/2000/1*cLQUX8jM2bMdwMcV2yXWYA.jpeg" alt="" />
        </DIV404>
    );
}

export default Undefined;
