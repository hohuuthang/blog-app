interface comment {
    author:{
        bio: string,
        following: string,
        image: string,
        username: string,
    }
    body: string,
    createdAt: string,
    id: string,
}

export default comment;