interface USER {
    bio: string,
    email: string,
    image: string,
    token: string,
    username: string,
}

export default USER;