export default interface NewArticle {
    title: string;
    body: string;
    description: string;
    tagList: string[];
}